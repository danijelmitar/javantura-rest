Spring REST Docs sample application
=============

Sample application used for demo in my
"Test driven documentation with Spring REST Docs" presentation given at JavanturaV4 conference.


Slides from presentation are available at:
http://www.slideshare.net/DanijelMitar/test-driven-documentation-with-spring-rest-docs