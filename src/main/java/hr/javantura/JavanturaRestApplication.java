package hr.javantura;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavanturaRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavanturaRestApplication.class, args);
	}

}
