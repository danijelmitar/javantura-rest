package hr.javantura.category;

import lombok.Data;

import javax.persistence.*;

/**
 * @author danijel.mitar
 */
@Data
@Entity
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

}
