package hr.javantura.category.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author danijel.mitar
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such Category!")
public class CategoryNotFoundException extends RuntimeException {

    public CategoryNotFoundException(String message) {
        super(message);
    }

}
