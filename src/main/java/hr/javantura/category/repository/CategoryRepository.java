package hr.javantura.category.repository;

import hr.javantura.category.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author danijel.mitar
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
}
