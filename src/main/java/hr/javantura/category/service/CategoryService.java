package hr.javantura.category.service;

import hr.javantura.category.Category;

import java.util.List;

/**
 * @author danijel.mitar
 */
public interface CategoryService {

    List<Category> fetchAll();

    Category fetchById(Long id);

}
