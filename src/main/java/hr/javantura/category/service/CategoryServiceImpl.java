package hr.javantura.category.service;

import hr.javantura.category.Category;
import hr.javantura.category.repository.CategoryRepository;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author danijel.mitar
 */
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class CategoryServiceImpl implements CategoryService {

    @NonNull
    private final CategoryRepository categoryRepository;

    @Override
    public List<Category> fetchAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category fetchById(Long id) {
        return categoryRepository.findOne(id);
    }

}
