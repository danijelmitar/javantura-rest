package hr.javantura.controller;

import hr.javantura.category.Category;
import hr.javantura.category.exception.CategoryNotFoundException;
import hr.javantura.category.service.CategoryService;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author danijel.mitar
 */
@RestController
@RequestMapping(value = "/categories")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class CategoryController {

    @NonNull
    private final CategoryService categoryService;

    @GetMapping
    public ResponseEntity<List<Category>> displayCategories() {
        List<Category> categories = categoryService.fetchAll();

        ResponseEntity<List<Category>> responseEntity;
        if (CollectionUtils.isEmpty(categories)) {
            responseEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            responseEntity = new ResponseEntity<>(categories, HttpStatus.OK);
        }

        return responseEntity;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Category> displayCategory(@PathVariable Long id) {
        Category category = categoryService.fetchById(id);

        if (category == null) {
            throw new CategoryNotFoundException("Category with id '" + id + "' not found");
        }

        return new ResponseEntity<>(category, HttpStatus.OK);
    }

}
