package hr.javantura.controller;

import hr.javantura.lecture.Lecture;
import hr.javantura.lecture.exception.LectureNotFoundException;
import hr.javantura.lecture.service.LectureService;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author danijel.mitar
 */
@RestController
@RequestMapping(value = "/lectures")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class LectureController {

    @NonNull
    private final LectureService lectureService;

    @GetMapping
    public ResponseEntity<List<Lecture>> displayLectures() {
        List<Lecture> lectureResources = lectureService.fetchAll();

        ResponseEntity<List<Lecture>> responseEntity;
        if (CollectionUtils.isEmpty(lectureResources)) {
            responseEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            responseEntity = new ResponseEntity<>(lectureResources, HttpStatus.OK);
        }

        return responseEntity;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Lecture> displayLecture(@PathVariable Long id) {
        Lecture lecture = lectureService.fetchById(id);

        if (lecture == null) {
            throw new LectureNotFoundException("Lecture with id '" + id + "' not found");
        }

        return new ResponseEntity<>(lecture, HttpStatus.OK);
    }

}
