package hr.javantura.controller;

import hr.javantura.lecturelevel.LectureLevel;
import hr.javantura.lecturelevel.exception.LectureLevelNotFoundException;
import hr.javantura.lecturelevel.service.LectureLevelService;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author danijel.mitar
 */
@RestController
@RequestMapping(value = "lecture-levels")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class LectureLevelController {

    @NonNull
    private final LectureLevelService lectureLevelService;

    @GetMapping
    public ResponseEntity<List<LectureLevel>> displayLectureLevels() {
        List<LectureLevel> lectureLevels = lectureLevelService.fetchAll();

        ResponseEntity<List<LectureLevel>> responseEntity;
        if (CollectionUtils.isEmpty(lectureLevels)) {
            responseEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            responseEntity = new ResponseEntity<>(lectureLevels, HttpStatus.OK);
        }

        return responseEntity;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<LectureLevel> displayLectureLevel(@PathVariable Long id) {
        LectureLevel lectureLevel = lectureLevelService.fetchById(id);

        if (lectureLevel == null) {
            throw new LectureLevelNotFoundException("Lecture level with id '" + id + "' not found");
        }

        return new ResponseEntity<>(lectureLevel, HttpStatus.OK);
    }

}
