package hr.javantura.controller;

import hr.javantura.lecturetype.LectureType;
import hr.javantura.lecturetype.LectureTypeNotFoundException;
import hr.javantura.lecturetype.service.LectureTypeService;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author danijel.mitar
 */
@RestController
@RequestMapping(value = "/lecture-types")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class LectureTypeController {

    @NonNull
    private final LectureTypeService lectureTypeService;

    @GetMapping
    public ResponseEntity<List<LectureType>> displayLectureTypes() {
        List<LectureType> lectureTypes = lectureTypeService.fetchAll();

        ResponseEntity<List<LectureType>> responseEntity;
        if (CollectionUtils.isEmpty(lectureTypes)) {
            responseEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            responseEntity = new ResponseEntity<>(lectureTypes, HttpStatus.OK);
        }

        return responseEntity;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<LectureType> displayLectureType(@PathVariable Long id) {
        LectureType lectureType = lectureTypeService.fetchById(id);

        if (lectureType == null) {
            throw new LectureTypeNotFoundException("Lecture with id '" + id + "' not found");
        }

        return new ResponseEntity<>(lectureType, HttpStatus.OK);
    }

}
