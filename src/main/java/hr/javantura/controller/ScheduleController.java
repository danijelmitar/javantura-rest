package hr.javantura.controller;

import hr.javantura.schedule.Schedule;
import hr.javantura.schedule.exception.ScheduleNotFoundException;
import hr.javantura.schedule.service.ScheduleService;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author danijel.mitar
 */
@RestController
@RequestMapping(value = "/schedules")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ScheduleController {

    @NonNull
    private final ScheduleService scheduleService;

    @GetMapping
    public ResponseEntity<List<Schedule>> displaySchedules() {
        List<Schedule> schedules = scheduleService.fetchAll();

        ResponseEntity<List<Schedule>> responseEntity;
        if (CollectionUtils.isEmpty(schedules)) {
            responseEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            responseEntity = new ResponseEntity<>(schedules, HttpStatus.OK);
        }

        return responseEntity;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Schedule> displaySchedule(@PathVariable Long id) {
        Schedule schedule = scheduleService.fetchById(id);

        if (schedule == null) {
            throw new ScheduleNotFoundException("Schedule with id '" + id + "' not found");
        }

        return new ResponseEntity<>(schedule, HttpStatus.OK);
    }

}
