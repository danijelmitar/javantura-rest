package hr.javantura.controller;

import hr.javantura.speaker.Speaker;
import hr.javantura.speaker.exception.SpeakerNotFoundException;
import hr.javantura.speaker.service.SpeakerService;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author danijel.mitar
 */
@RestController
@RequestMapping(value = "/speakers")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class SpeakerController {

    @NonNull
    private final SpeakerService speakerService;

    @GetMapping
    public ResponseEntity<List<Speaker>> displaySpeakers() {
        List<Speaker> speakers = speakerService.fetchAll();

        ResponseEntity<List<Speaker>> responseEntity;
        if (CollectionUtils.isEmpty(speakers)) {
            responseEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            responseEntity = new ResponseEntity<>(speakers, HttpStatus.OK);
        }

        return responseEntity;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Speaker> displaySpeaker(@PathVariable Long id) {
        Speaker speaker = speakerService.fetchById(id);

        if (speaker == null) {
            throw new SpeakerNotFoundException("Speaker with id '" + id + "' not found");
        }

        return new ResponseEntity<>(speaker, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Speaker> createSpeaker(@RequestBody Speaker speaker) {
        return new ResponseEntity<>(speakerService.create(speaker), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Speaker> putSpeaker(@PathVariable Long id, @RequestBody @Valid Speaker speaker) {
        ResponseEntity<Speaker> responseEntity;
        if (speakerService.fetchById(id) == null) {
            responseEntity = new ResponseEntity<>(speakerService.create(speaker), HttpStatus.OK);
        } else {
            speakerService.update(id, speaker);
            responseEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return responseEntity;
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteSpeaker(@PathVariable Long id) {
        speakerService.delete(id);
    }

}
