package hr.javantura.controller;

import hr.javantura.track.Track;
import hr.javantura.track.exception.TrackNotFoundException;
import hr.javantura.track.service.TrackService;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author danijel.mitar
 */
@RestController
@RequestMapping(value = "/tracks")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class TrackController {

    @NonNull
    private final TrackService trackService;

    @GetMapping
    public ResponseEntity<List<Track>> displayTracks() {
        List<Track> tracks = trackService.fetchAll();

        ResponseEntity<List<Track>> responseEntity;
        if (CollectionUtils.isEmpty(tracks)) {
            responseEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            responseEntity = new ResponseEntity<>(tracks, HttpStatus.OK);
        }

        return responseEntity;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Track> displayTrack(@PathVariable Long id) {
        Track track = trackService.fetchById(id);

        if (track == null) {
            throw new TrackNotFoundException("Track with id '" + id + "' not found");
        }

        return new ResponseEntity<>(track, HttpStatus.OK);
    }

}
