package hr.javantura.converter;

import hr.javantura.lecturelevel.LectureLevel;

import javax.persistence.AttributeConverter;

/**
 * @author danijel.mitar
 */
public class LectureLevelAttributeConverter implements AttributeConverter<LectureLevel, Long> {

    @Override
    public Long convertToDatabaseColumn(LectureLevel attribute) {
        Long id = null;

        if (attribute != null) {
            id = attribute.getId();
        }

        return id;
    }

    @Override
    public LectureLevel convertToEntityAttribute(Long dbData) {
        return LectureLevel.fromId(dbData);
    }

}
