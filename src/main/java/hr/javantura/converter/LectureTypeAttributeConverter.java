package hr.javantura.converter;

import hr.javantura.lecturetype.LectureType;

import javax.persistence.AttributeConverter;

/**
 * @author danijel.mitar
 */
public class LectureTypeAttributeConverter implements AttributeConverter<LectureType, Long> {

    @Override
    public Long convertToDatabaseColumn(LectureType attribute) {
        Long id = null;

        if (attribute != null) {
            id = attribute.getId();
        }

        return id;
    }

    @Override
    public LectureType convertToEntityAttribute(Long dbData) {
        return LectureType.fromId(dbData);
    }

}
