package hr.javantura.lecture;

import com.fasterxml.jackson.annotation.JsonIgnore;
import hr.javantura.converter.LectureLevelAttributeConverter;
import hr.javantura.converter.LectureTypeAttributeConverter;
import hr.javantura.lecturelevel.LectureLevel;
import hr.javantura.lecturetype.LectureType;
import hr.javantura.speaker.Speaker;
import hr.javantura.category.Category;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * @author danijel.mitar
 */
@Data
@Entity
public class Lecture {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column(length = 1024)
    private String description;

    @ManyToOne
    private Category category;

    @Column(name = "lecture_type_id")
    @Convert(converter = LectureTypeAttributeConverter.class)
    private LectureType lectureType;

    @Column(name = "lecture_level_id")
    @Convert(converter = LectureLevelAttributeConverter.class)
    private LectureLevel lectureLevel;

    @JsonIgnore
    @ManyToMany(mappedBy = "lectures")
    private List<Speaker> speakers;

    @Override
    public String toString() {
        return this.name;
    }

}
