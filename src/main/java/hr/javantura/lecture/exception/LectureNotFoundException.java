package hr.javantura.lecture.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author danijel.mitar
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such Lecture!")
public class LectureNotFoundException extends RuntimeException {

    public LectureNotFoundException(String message) {
        super(message);
    }

}
