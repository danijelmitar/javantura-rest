package hr.javantura.lecture.repository;

import hr.javantura.lecture.Lecture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author danijel.mitar
 */
@Repository
public interface LectureRepository extends JpaRepository<Lecture, Long> {
}
