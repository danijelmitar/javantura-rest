package hr.javantura.lecture.service;

import hr.javantura.lecture.Lecture;

import java.util.List;

/**
 * @author danijel.mitar
 */
public interface LectureService {

    List<Lecture> fetchAll();

    Lecture fetchById(Long id);

}
