package hr.javantura.lecture.service;

import hr.javantura.lecture.Lecture;
import hr.javantura.lecture.repository.LectureRepository;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author danijel.mitar
 */
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class LectureServiceImpl implements LectureService {

    @NonNull
    private final LectureRepository lectureRepository;

    @Override
    public List<Lecture> fetchAll() {
        return lectureRepository.findAll();
    }

    @Override
    public Lecture fetchById(Long id) {
        return lectureRepository.findOne(id);
    }

}
