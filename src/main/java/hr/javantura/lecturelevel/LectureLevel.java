package hr.javantura.lecturelevel;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.Maps;
import hr.javantura.lecturelevel.serialization.LectureLevelDeserializer;
import hr.javantura.lecturelevel.serialization.LectureLevelSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;

/**
 * @author danijel.mitar
 */
@Getter
@AllArgsConstructor
@JsonSerialize(using = LectureLevelSerializer.class)
@JsonDeserialize(using = LectureLevelDeserializer.class)
public enum LectureLevel {

    GENERAL (1L, "General"),
    VERY_DETAILED (2L, "Very detailed"),
    IN_BETWEEN (3L, "Something in between");

    private final Long id;
    private final String name;

    private static final Map<Long, LectureLevel> LOOKUP_BY_ID = Maps.uniqueIndex(
            Arrays.asList(LectureLevel.values()),
            LectureLevel::getId
    );

    public static LectureLevel fromId(Long id) {
        return LOOKUP_BY_ID.get(id);
    }

}
