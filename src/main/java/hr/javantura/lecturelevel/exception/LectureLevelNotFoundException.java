package hr.javantura.lecturelevel.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author danijel.mitar
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such Lecture level!")
public class LectureLevelNotFoundException extends RuntimeException {

    public LectureLevelNotFoundException(String message) {
        super(message);
    }

}
