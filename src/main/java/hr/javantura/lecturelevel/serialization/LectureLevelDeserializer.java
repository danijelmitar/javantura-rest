package hr.javantura.lecturelevel.serialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import hr.javantura.lecturelevel.LectureLevel;

import java.io.IOException;

/**
 * @author danijel.mitar
 */
public class LectureLevelDeserializer extends StdDeserializer<LectureLevel> {

    public LectureLevelDeserializer() {
        super(LectureLevel.class);
    }

    protected LectureLevelDeserializer(Class<LectureLevel> t) {
        super(t);
    }

    @Override
    public LectureLevel deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        Long id = node.get("id").longValue();

        return LectureLevel.fromId(id);
    }
}
