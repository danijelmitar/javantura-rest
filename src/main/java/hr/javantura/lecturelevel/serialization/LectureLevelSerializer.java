package hr.javantura.lecturelevel.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import hr.javantura.lecturelevel.LectureLevel;

import java.io.IOException;

/**
 * @author danijel.mitar
 */
public class LectureLevelSerializer extends StdSerializer<LectureLevel> {

    public LectureLevelSerializer() {
        super(LectureLevel.class);
    }

    protected LectureLevelSerializer(Class<LectureLevel> t) {
        super(t);
    }

    @Override
    public void serialize(LectureLevel lectureLevel, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeFieldName("id");
        jsonGenerator.writeNumber(lectureLevel.getId());
        jsonGenerator.writeFieldName("name");
        jsonGenerator.writeString(lectureLevel.getName());
        jsonGenerator.writeEndObject();
    }
}
