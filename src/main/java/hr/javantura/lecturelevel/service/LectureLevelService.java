package hr.javantura.lecturelevel.service;

import hr.javantura.lecturelevel.LectureLevel;

import java.util.List;

/**
 * @author danijel.mitar
 */
public interface LectureLevelService {

    List<LectureLevel> fetchAll();

    LectureLevel fetchById(Long id);

}
