package hr.javantura.lecturelevel.service;

import hr.javantura.lecturelevel.LectureLevel;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * @author danijel.mitar
 */
@Service
public class LectureLevelServiceImpl implements LectureLevelService {

    @Override
    public List<LectureLevel> fetchAll() {
        return Arrays.asList(LectureLevel.values());
    }

    @Override
    public LectureLevel fetchById(Long id) {
        return LectureLevel.fromId(id);
    }

}
