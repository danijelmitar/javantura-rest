package hr.javantura.lecturetype;

import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.Maps;
import hr.javantura.lecturetype.serialization.LectureTypeDeserializer;
import hr.javantura.lecturetype.serialization.LectureTypeSerializer;
import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.Map;

/**
 * @author danijel.mitar
 */
@AllArgsConstructor
@JsonSerialize(using = LectureTypeSerializer.class)
@JsonDeserialize(using = LectureTypeDeserializer.class)
public enum LectureType {

    PRESENTATION (1L, "Presentation"),
    PRESENTATION_WITH_DEMO (2L, "Presentation + demonstration");

    private final Long id;
    private final String name;

    private static final Map<Long, LectureType> LOOKUP_BY_ID = Maps.uniqueIndex(
            Arrays.asList(LectureType.values()),
            LectureType::getId
    );

    public static LectureType fromId(Long id) {
        return LOOKUP_BY_ID.get(id);
    }

    public Long getId() {
        return this.id;
    }

    @JsonValue
    public String getName() {
        return this.name;
    }

}
