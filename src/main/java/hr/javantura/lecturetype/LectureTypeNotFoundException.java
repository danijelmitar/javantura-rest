package hr.javantura.lecturetype;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author danijel.mitar
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such Lecture type!")
public class LectureTypeNotFoundException extends RuntimeException {

    public LectureTypeNotFoundException(String message) {
        super(message);
    }

}
