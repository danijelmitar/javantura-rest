package hr.javantura.lecturetype.serialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import hr.javantura.lecturetype.LectureType;

import java.io.IOException;

/**
 * @author danijel.mitar
 */
public class LectureTypeDeserializer extends StdDeserializer<LectureType> {

    public LectureTypeDeserializer() {
        super(LectureType.class);
    }

    protected LectureTypeDeserializer(Class<LectureType> t) {
        super(t);
    }

    @Override
    public LectureType deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        Long id = node.get("id").longValue();

        return LectureType.fromId(id);
    }

}
