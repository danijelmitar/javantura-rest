package hr.javantura.lecturetype.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import hr.javantura.lecturetype.LectureType;

import java.io.IOException;

/**
 * @author danijel.mitar
 */
public class LectureTypeSerializer extends StdSerializer<LectureType> {

    public LectureTypeSerializer() {
        super(LectureType.class);
    }

    protected LectureTypeSerializer(Class<LectureType> t) {
        super(t);
    }

    @Override
    public void serialize(LectureType lectureType, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeFieldName("id");
        jsonGenerator.writeNumber(lectureType.getId());
        jsonGenerator.writeFieldName("name");
        jsonGenerator.writeString(lectureType.getName());
        jsonGenerator.writeEndObject();
    }

}
