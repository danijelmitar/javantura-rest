package hr.javantura.lecturetype.service;

import hr.javantura.lecturetype.LectureType;

import java.util.List;

/**
 * @author danijel.mitar
 */
public interface LectureTypeService {

    List<LectureType> fetchAll();

    LectureType fetchById(Long id);

}
