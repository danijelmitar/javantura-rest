package hr.javantura.lecturetype.service;

import hr.javantura.lecturetype.LectureType;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * @author danijel.mitar
 */
@Service
public class LectureTypeServiceImpl implements LectureTypeService {

    @Override
    public List<LectureType> fetchAll() {
        return Arrays.asList(LectureType.values());
    }

    @Override
    public LectureType fetchById(Long id) {
        return LectureType.fromId(id);
    }

}
