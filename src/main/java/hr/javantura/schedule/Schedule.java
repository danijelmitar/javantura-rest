package hr.javantura.schedule;

import hr.javantura.track.Track;
import hr.javantura.track.converter.TrackAttributeConverter;
import hr.javantura.lecture.Lecture;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author danijel.mitar
 */
@Data
@Entity
public class Schedule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private LocalDateTime startTime;

    @Column
    private LocalDateTime endTime;

    @ManyToOne
    private Lecture lecture;

    @Convert(converter = TrackAttributeConverter.class)
    private Track track;

}
