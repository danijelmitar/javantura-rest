package hr.javantura.schedule.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author danijel.mitar
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such Schedule!")
public class ScheduleNotFoundException extends RuntimeException {

    public ScheduleNotFoundException(String message) {
        super(message);
    }

}
