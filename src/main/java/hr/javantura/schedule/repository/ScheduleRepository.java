package hr.javantura.schedule.repository;

import hr.javantura.schedule.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author danijel.mitar
 */
@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Long> {
}
