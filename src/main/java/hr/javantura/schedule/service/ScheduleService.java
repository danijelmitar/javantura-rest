package hr.javantura.schedule.service;

import hr.javantura.schedule.Schedule;

import java.util.List;

/**
 * @author danijel.mitar
 */
public interface ScheduleService {

    List<Schedule> fetchAll();

    Schedule fetchById(Long id);

}
