package hr.javantura.schedule.service;

import hr.javantura.schedule.Schedule;
import hr.javantura.schedule.repository.ScheduleRepository;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author danijel.mitar
 */
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ScheduleServiceImpl implements ScheduleService {

    @NonNull
    private final ScheduleRepository scheduleRepository;

    @Override
    public List<Schedule> fetchAll() {
        return scheduleRepository.findAll();
    }

    @Override
    public Schedule fetchById(Long id) {
        return scheduleRepository.findOne(id);
    }

}
