package hr.javantura.speaker;

import com.fasterxml.jackson.annotation.JsonIgnore;
import hr.javantura.lecture.Lecture;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author danijel.mitar
 */
@Data
@Entity
@Table(name = "speaker")
public class Speaker {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @NotNull
    private String name;

    @Column(length = 512)
    private String biography;

    @ManyToMany
    @JoinTable(name="Speaker_Lecture",
            joinColumns = {
                @JoinColumn(name = "speakerId")
            },
            inverseJoinColumns = {
                @JoinColumn(name = "lectureId")
            }
    )
    @JsonIgnore
    private List<Lecture> lectures;

}
