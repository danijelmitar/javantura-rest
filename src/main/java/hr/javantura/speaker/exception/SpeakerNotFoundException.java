package hr.javantura.speaker.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author danijel.mitar
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such Speaker!")
public class SpeakerNotFoundException extends RuntimeException {

    public SpeakerNotFoundException(String message) {
        super(message);
    }

}
