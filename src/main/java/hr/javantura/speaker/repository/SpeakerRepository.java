package hr.javantura.speaker.repository;

import hr.javantura.speaker.Speaker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author danijel.mitar
 */
@Repository
public interface SpeakerRepository extends JpaRepository<Speaker, Long> {
}
