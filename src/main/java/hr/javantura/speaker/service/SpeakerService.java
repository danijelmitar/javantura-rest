package hr.javantura.speaker.service;

import hr.javantura.speaker.Speaker;

import java.util.List;

/**
 * @author danijel.mitar
 */
public interface SpeakerService {

    List<Speaker> fetchAll();

    Speaker fetchById(Long id);
    Speaker create(Speaker speaker);

    void update(Long id, Speaker speaker);
    void delete(Long id);

}
