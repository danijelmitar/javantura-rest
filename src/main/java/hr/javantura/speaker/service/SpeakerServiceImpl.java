package hr.javantura.speaker.service;

import hr.javantura.speaker.Speaker;
import hr.javantura.speaker.exception.SpeakerNotFoundException;
import hr.javantura.speaker.repository.SpeakerRepository;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author danijel.mitar
 */
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class SpeakerServiceImpl implements SpeakerService {

    @NonNull
    private final SpeakerRepository speakerRepository;

    @Override
    public List<Speaker> fetchAll() {
        return speakerRepository.findAll();
    }

    @Override
    public Speaker fetchById(Long id) {
        return speakerRepository.findOne(id);
    }

    @Override
    public Speaker create(Speaker speaker) {
        return speakerRepository.save(speaker);
    }

    @Override
    public void update(Long id, Speaker speaker) {
        speaker.setId(id);
        speakerRepository.save(speaker);
    }

    @Override
    public void delete(Long id) {
        try {
            speakerRepository.delete(id);
        } catch (EmptyResultDataAccessException ex) {
            throw new SpeakerNotFoundException("Speaker with id '" + id + "' not found");
        }
    }

}
