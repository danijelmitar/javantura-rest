package hr.javantura.track;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.Maps;
import hr.javantura.track.serializer.TrackSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;

/**
 * @author danijel.mitar
 */
@Getter
@AllArgsConstructor
@JsonSerialize(using = TrackSerializer.class)
public enum Track {

    A (1L, "Hall A", 100L),
    B (2L, "Hall B", 100L),
    C (3L, "Hall C", 100L);

    private final Long id;
    private final String name;
    private final Long capacity;

    private static final Map<Long, Track> LOOKUP_BY_ID = Maps.uniqueIndex(
            Arrays.asList(Track.values()),
            Track::getId
    );

    public static Track fromId(Long id) {
        return LOOKUP_BY_ID.get(id);
    }

}
