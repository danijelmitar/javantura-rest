package hr.javantura.track.converter;

import hr.javantura.track.Track;

import javax.persistence.AttributeConverter;

/**
 * @author danijel.mitar
 */
public class TrackAttributeConverter implements AttributeConverter<Track, Long> {

    @Override
    public Long convertToDatabaseColumn(Track attribute) {
        Long id = null;

        if (attribute != null) {
            id = attribute.getId();
        }

        return id;
    }

    @Override
    public Track convertToEntityAttribute(Long dbData) {
        return Track.fromId(dbData);
    }

}
