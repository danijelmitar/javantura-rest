package hr.javantura.track.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import hr.javantura.track.Track;

import java.io.IOException;

/**
 * @author danijel.mitar
 */
public class TrackSerializer extends StdSerializer<Track> {

    public TrackSerializer() {
        super(Track.class);
    }

    protected TrackSerializer(Class<Track> t) {
        super(t);
    }

    @Override
    public void serialize(Track track, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeFieldName("id");
        jsonGenerator.writeNumber(track.getId());
        jsonGenerator.writeFieldName("name");
        jsonGenerator.writeString(track.getName());
        jsonGenerator.writeFieldName("capacity");
        jsonGenerator.writeNumber(track.getCapacity());
        jsonGenerator.writeEndObject();
    }

}
