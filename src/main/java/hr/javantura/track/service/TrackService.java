package hr.javantura.track.service;

import hr.javantura.track.Track;

import java.util.List;

/**
 * @author danijel.mitar
 */
public interface TrackService {

    List<Track> fetchAll();

    Track fetchById(Long id);

}
