package hr.javantura.track.service;

import hr.javantura.track.Track;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * @author danijel.mitar
 */
@Service
public class TrackServiceImpl implements TrackService {

    @Override
    public List<Track> fetchAll() {
        return Arrays.asList(Track.values());
    }

    @Override
    public Track fetchById(Long id) {
        return Track.fromId(id);
    }

}
