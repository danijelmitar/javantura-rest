INSERT INTO category (id, name)
VALUES (1, 'Web and Mobile Development'),
  (2, 'Methodologies and Tools'),
  (3, 'Trends, Future, and Community'),
  (4, 'Keynote'),
  (5, 'Core Java, Frameworks, and Servers');

INSERT INTO speaker (id, name, biography)
VALUES (1, 'Danijel Mitar', 'Danijel Mitar is currently working as Java DevOps at KING ICT. He enjoys programming, optimizing and tuning Java applications'),
  (2, 'Aleksander Radovan', 'Aleksander Radovan graduated in 2004. at the Faculty of Electrical Engineering and Computer Science and is currently finishing the PhD program. He is in the Java more that 12 years. He works for KING ICT as Java team lead and beside this he teaches at several universities in and around Zagreb courses related to programming in Java and information systems in general. He is also interested in areas such as artificial intelligence, genetic algorithms, robotics and teaching the younger generation programming.'),
  (3, 'Roko Roić', 'Roko Roić has more than 15y in the IT industry. Started of as an enterprise Java developer. Had a lot to do with system infrastructure and performance optimisation on a large scale. Moved on to managing teams and projects in developing IT solutions. He wrote a book on Agile and Lean, first of this kind in Croatian language.'),
  (4, 'Josip Kovaček', 'Završio sam Tehničko Veleučilište u Zagrebu, smjer Programsko inženjerstvo gdje sam se upoznao i zainteresirao za Java programski jezik. Od tada radim kao Java developer u tvrtci KING ICT.');

INSERT INTO lecture (id, name, description, category_id, lecture_type_id, lecture_level_id)
VALUES (1, 'Test-driven documentation with Spring REST Docs', 'Developers realize that documentation is important, but they don’t like to write documentation – it’s boring and time consuming. Goal of this presentation is to show how you can write documentation without actually writing it. With help of Spring REST Docs and TDD, your tests will generate documentation for you!', 1, 2, 2),
  (2, 'Java lambdas and stream – are they better than for loops?', 'Should we use lambda expressions and streams instead of using common things like for loops? Which approach gives better performance in case of using collections with fewer objects and which with collections that contain big number of objects? Are the fancy „one liner“ solutions better (faster) than the regular loops or only shorter? The author will try to answer these questions and show few examples after which you can decide when to preferr using lambdas and streams.', 5, 1, 3),
  (3, 'Let me tell you a story of why Scrum is not for you', 'Scrum is a great hammer, but not every process is a nail. What if you have a screw? You probably need a screwdriver, not a hammer. I’ll explain the anti-patterns of using Scrum and outline other methodologies that can be a better fit for some cases.', 2, 1, 2),
  (4, 'Spring Boot and JavaFX – can they play together?', 'No doubt that web applications are dominating by popularity but desktop applications have not gone anywhere yet. Which way (and technologies) to choose when there is requirement to build closed – system RIA desktop application in a rapid manner with thoughts on years of future maintenance? In Java land Spring Boot is a good bet with its convention over configuration approach which helps to get the project up and running as quickly as possible, in early development stage as well as in maintenance days years later. For GUI part, Java language already offers built-in JavaFX package for building RIA applications and it’s also a good bet that it won’t go anywhere near soon. Now if we could just make them play together and pick the best from both worlds…', 5, 2, 3);

INSERT INTO speaker_lecture (speaker_id, lecture_id)
VALUES (1, 1), (2, 2), (3, 3), (4, 4);