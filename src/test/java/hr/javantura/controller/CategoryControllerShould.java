package hr.javantura.controller;

import hr.javantura.category.Category;
import hr.javantura.category.service.CategoryService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author danijel.mitar
 */
@RunWith(SpringRunner.class)
@WebMvcTest(CategoryController.class)
public class CategoryControllerShould {

    @Rule
    public JUnitRestDocumentation restDocumentation =
            new JUnitRestDocumentation("target/generated-snippets");

    private RestDocumentationResultHandler documentationResultHandler;

    private MockMvc mockMvc;

    @MockBean
    private CategoryService categoryService;

    @Autowired
    private WebApplicationContext context;

    @Before
    public void setUp() throws Exception {
        this.documentationResultHandler = document("{method-name}", preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint()));
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation)).alwaysDo(documentationResultHandler)
                .build();
    }

    @Test
    public void returnAllCategories_onDisplayCategories_whenAnyExists() throws Exception {
        Category category = new Category();
        category.setId(1L);
        category.setName("Test category");

        when(this.categoryService.fetchAll()).thenReturn(Collections.singletonList(category));

        this.mockMvc.perform(get("/categories").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(this.documentationResultHandler.document(responseFields(
                        fieldWithPath("[].id").description("Category id").type(JsonFieldType.NUMBER),
                        fieldWithPath("[].name").description("Category name").type(JsonFieldType.STRING)
                )));
    }

    @Test
    public void returnNoContent_onDisplayCategories_whenNoneExists() throws Exception {
        when(this.categoryService.fetchAll()).thenReturn(Collections.emptyList());

        this.mockMvc.perform(get("/categories").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
                .andDo(this.documentationResultHandler.document());
    }

    @Test
    public void returnCategory_onGetCategoryWithId_whenExists() throws Exception {
        Category category = new Category();
        category.setId(1L);
        category.setName("Test category");

        when(this.categoryService.fetchById(category.getId())).thenReturn(category);

        this.mockMvc.perform(get("/categories/{id}", category.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(this.documentationResultHandler.document(responseFields(
                        fieldWithPath("id").description("Category id").type(JsonFieldType.NUMBER),
                        fieldWithPath("name").description("Category name").type(JsonFieldType.STRING)
                )));
    }

    @Test
    public void returnNotFound_onGetCategoryWithId_whenNotExists() throws Exception {
        Long unexistingCategoryId = 0L;

        when(this.categoryService.fetchById(unexistingCategoryId)).thenReturn(null);

        this.mockMvc.perform(get("/categories/{id}", unexistingCategoryId).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andDo(this.documentationResultHandler.document());
    }

}