package hr.javantura.controller;

import hr.javantura.category.Category;
import hr.javantura.lecture.Lecture;
import hr.javantura.lecture.service.LectureService;
import hr.javantura.lecturelevel.LectureLevel;
import hr.javantura.lecturetype.LectureType;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * @author danijel.mitar
 */
@RunWith(SpringRunner.class)
@WebMvcTest(LectureController.class)
public class LectureControllerShould {

    @Rule
    public JUnitRestDocumentation restDocumentation =
            new JUnitRestDocumentation("target/generated-snippets");

    private RestDocumentationResultHandler documentationResultHandler;

    private MockMvc mockMvc;

    @MockBean
    private LectureService lectureService;

    @Autowired
    private WebApplicationContext context;

    @Before
    public void setUp() {
        this.documentationResultHandler = document("{method-name}", preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint()));
        this.mockMvc = webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation)).alwaysDo(documentationResultHandler)
                .build();
    }

    @Test
    public void returnAllLectures_onGetLectures_whenAnyExists() throws Exception {
        // given
        Lecture lecture = getLecture();

        when(this.lectureService.fetchAll()).thenReturn(Collections.singletonList(lecture));

        this.mockMvc.perform(MockMvcRequestBuilders.get("/lectures").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(documentationResultHandler.document(responseFields(
                        fieldWithPath("[].id").description("Lecture id").type(JsonFieldType.NUMBER),
                        fieldWithPath("[].name").description("Lecture name").type(JsonFieldType.STRING),
                        fieldWithPath("[].category").description("Lecture category").type(JsonFieldType.OBJECT),
                        fieldWithPath("[].lectureType").description("Lecture type").type(JsonFieldType.OBJECT),
                        fieldWithPath("[].lectureLevel").description("Lecture level").type(JsonFieldType.OBJECT),
                        fieldWithPath("[].description").description("Lecture description").type(JsonFieldType.STRING)
                )));
    }

    private Lecture getLecture() {
        Lecture lecture = new Lecture();
        lecture.setId(1L);
        lecture.setName("Spring REST Docs");
        lecture.setDescription("Some description");
        lecture.setLectureType(LectureType.PRESENTATION_WITH_DEMO);
        lecture.setLectureLevel(LectureLevel.VERY_DETAILED);

        Category category = new Category();
        category.setName("Java Web technologies");
        lecture.setCategory(category);

        return lecture;
    }

}
