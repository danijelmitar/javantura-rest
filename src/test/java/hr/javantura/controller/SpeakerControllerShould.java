package hr.javantura.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import hr.javantura.speaker.Speaker;
import hr.javantura.speaker.exception.SpeakerNotFoundException;
import hr.javantura.speaker.service.SpeakerService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * @author danijel.mitar
 */
@RunWith(SpringRunner.class)
@WebMvcTest(SpeakerController.class)
public class SpeakerControllerShould {

    @Rule
    public JUnitRestDocumentation restDocumentation =
            new JUnitRestDocumentation("target/generated-snippets");

    private RestDocumentationResultHandler documentationResultHandler;

    private MockMvc mockMvc;

    @MockBean
    private SpeakerService speakerService;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @Before
    public void setUp() throws Exception {
        this.documentationResultHandler = document("{method-name}", preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint()));
        this.mockMvc = webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation)).alwaysDo(documentationResultHandler)
                .build();
    }

    @Test
    public void returnAllSpeakers_onDisplaySpeakers_whenAnyExists() throws Exception {
        Speaker speaker = new Speaker();
        speaker.setId(1L);
        speaker.setName("John Doe");
        speaker.setBiography("No bio available");

        when(this.speakerService.fetchAll()).thenReturn(Collections.singletonList(speaker));

        this.mockMvc.perform(get("/speakers").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(this.documentationResultHandler.document(responseFields(
                        fieldWithPath("[].id").description("Speaker id").type(JsonFieldType.NUMBER),
                        fieldWithPath("[].name").description("Speaker name").type(JsonFieldType.STRING),
                        fieldWithPath("[].biography").description("Speaker biography").type(JsonFieldType.STRING)
                )));
    }

    @Test
    public void returnNoContent_onDisplaySpeakers_whenNoneExists() throws Exception {
        when(this.speakerService.fetchAll()).thenReturn(Collections.emptyList());

        this.mockMvc.perform(get("/speakers").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
                .andDo(this.documentationResultHandler.document());
    }

    @Test
    public void returnCreatedSpeaker_onPostNewSpeaker() throws Exception {
        Map<String, String> speaker = new HashMap<>();
        speaker.put("name", "John Doe");
        speaker.put("biography", "No bio available");

        Speaker createdSpeaker = new Speaker();
        createdSpeaker.setName("John Doe");
        createdSpeaker.setBiography("No bio available");
        createdSpeaker.setId(1L);
        when(this.speakerService.create(any())).thenReturn(createdSpeaker);

        this.mockMvc.perform(post("/speakers").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(speaker)).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andDo(this.documentationResultHandler.document(requestFields(
                        fieldWithPath("name").description("Speaker full name").type(JsonFieldType.STRING),
                        fieldWithPath("biography").description("Speaker biography").type(JsonFieldType.STRING)
                ), responseFields(
                        fieldWithPath("id").description("Speaker id").type(JsonFieldType.NUMBER),
                        fieldWithPath("name").description("Speaker full name").type(JsonFieldType.STRING),
                        fieldWithPath("biography").description("Speaker biography").type(JsonFieldType.STRING)
                )));
    }

    @Test
    public void returnNoContent_onSuccessfulUpdateSpeaker() throws Exception {
        Long speakerId = 1L;
        Map<String, String> speaker = new HashMap<>();
        speaker.put("name", "John Doe");
        speaker.put("biography", "No bio available");

        when(this.speakerService.fetchById(speakerId)).thenReturn(new Speaker());
        doNothing().when(this.speakerService).update(anyLong(), any());

        this.mockMvc.perform(put("/speakers/{id}", speakerId).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(speaker)).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
                .andDo(this.documentationResultHandler.document(requestFields(
                        fieldWithPath("name").description("Speaker full name").type(JsonFieldType.STRING),
                        fieldWithPath("biography").description("Speaker biography").type(JsonFieldType.STRING)
                )));
    }

    @Test
    public void returnStatusOk_onDeleteExistingSpeaker() throws Exception {
        doNothing().when(this.speakerService).delete(anyLong());

        this.mockMvc.perform(delete("/speakers/{id}", 1L).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void returnStatusNotFound_onDeleteUnexistingSpeaker() throws Exception {
        doThrow(SpeakerNotFoundException.class).when(this.speakerService).delete(anyLong());

        this.mockMvc.perform(delete("/speakers/{id}", 1L).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

}