package hr.javantura.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author danijel.mitar
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class RepositoryIT {

    @Test
    public void repositoryLayerLoads() {
    }

}